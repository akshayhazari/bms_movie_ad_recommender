import scrapy
from scrapy.crawler import CrawlerProcess
from bookmyshow_spider import BookMyShow
import os,uuid,sys,warnings
from scrapy.utils.project import get_project_settings
warnings.filterwarnings('ignore')

class BookMyS(object):

    def call(self,start_url):
        process = CrawlerProcess(get_project_settings())
        process.crawl('bookmyshow',start_url=start_url)
        process.start()
        
if __name__ == "__main__":
    sitemap = BookMyS().call(sys.argv[1])
