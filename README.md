# SiteMapGenerator

> Installation

          pip install -r requirements.txt


> Run Program to crawl movies , events , sports, activities , plays

         ./run <url>

         example

         ./run http://www.bookmyshow.com/bengaluru

> Output Generated

         output file is generated in data directory crawling movies , events , sports , plays , activities

> Run ./data_preparation.py and ./movies_aggregator.py to generate movies aggregated data.

> Run reviews.py to crawl user review data 

> Run user_data.py (for user review data preparation) followed by user_aggregator.py (to aggregate review data for users based on movie details) 
assumptions
         > gender female = all names ending with "i", "e", "a", "y","reet","al" are female.
         > hasbooked = generate random variable to assign user for each movie review (assumption that he has booked the movie using bookmyshow) 

> advertisements.txt - created manually and have affinity to genders , languages, movie genres

> similarity_search.py - uses nmslib to get nearest neighbours of each user 
                           Based on the neighbours we get the movie recommendations for people who are similar to the current user 
                           and who are likely to watch other movies.
                           
                           Finally we generate recommendations based on whther a person has booked earlier.
                           If booked a ticket earlier then we show him movie recommendations
                           else we show him advertisement recommendations (by generating a random function to assign probability to 
                           different genres, language , type based on the movies he watched and reviewed and other user traits)

> ./similarity_search.py to run 

