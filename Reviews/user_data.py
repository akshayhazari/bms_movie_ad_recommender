import csv,re,numpy,sys

argv = sys.argv

if len(argv)<2:
    print "Please provide input file"

reviews=open(argv[1])
reviews = csv.reader(reviews,delimiter='~')
writer = csv.writer(open(argv[1].split(".")[0]+"_parsed.txt","w+"),delimiter="~")
header,hflag = [],0

def get_random():
    return str(numpy.random.choice(numpy.arange(0,2),p=[0.7,0.3]))

for review in reviews:
    review = map(lambda x : x.split(":") ,review)
    for i in review:
        if not hflag:
            header+=[i[0]]
        if i[0] == "description":
            language = re.findall("marathi|hindi|kanada|tamil|telugu|punjabi|gujrati|bengali", i[1], re.IGNORECASE)
            language = language[0] if language else ""
            location = re.findall("bengaluru|pune|mumbai|chennai|chandigarh|ahmedabad|hyderabad|ncr", i[1],
                                  re.IGNORECASE)
            location = location[0] if location else ""
        elif i[0] =="username":
            i[1] = i[1].lower()
            if i[0] == "username":
                i[1] = i[1].lower()
                name = i[1].split()
                gender = "m"
                if name[0].endswith(("i", "e", "a", "y","reet","al")):
                    gender = "f"
    review[-2] = ["language",language]
    review[-1] = ["location",location]
    review.append(["hasbooked",get_random()])
    review.append(["gender", gender])
    if not hflag:
        header = filter(None,header)
        header += ["location","hasbooked","gender"]
        writer.writerow(header)
        hflag = 1
    writer.writerow(map(lambda x:x[1],review))
